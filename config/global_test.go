package config

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	gcp "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/gcp/config"
	orka "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/orka/config"
)

func TestLoadFromFile(t *testing.T) {
	const cfg = `
Provider = "TestProvider"
Executor = "TestExecutor"
OS = "TestOS"

LogLevel = "TestLogLevel"
LogFile = "TestLogFile"
LogFormat = "TestLogFormat"

VMTag = "TestVMTag"

[ProviderCache]
  Enabled = true
  Directory = "/test/dir"

[GCP]
  Tags = ["TestTag1", "TestTag2"]
  Username = "TestUsername"
  ServiceAccountFile = "path/to/file"
  Project = "project"
  Zone = "zone"
  MachineType = "machine-type"
  MinCPUPlatform = "machine-cpu-platform"
  Image = "image"
  DiskSize = 10
  DiskType = "disk-type"
  Network = "network"
  Subnetwork = "subnetwork"
  UseInternalIPOnly = true

[Orka]
`

	expectedCfg := Global{
		Provider: "TestProvider",
		OS:       "TestOS",
		ProviderCache: &ProviderCache{
			Enabled:   true,
			Directory: "/test/dir",
		},
		LogLevel:  "TestLogLevel",
		LogFile:   "TestLogFile",
		LogFormat: "TestLogFormat",
		VMTag:     "TestVMTag",
		GCP: gcp.Provider{
			Tags:               []string{"TestTag1", "TestTag2"},
			Username:           "TestUsername",
			ServiceAccountFile: "path/to/file",
			Project:            "project",
			Zone:               "zone",
			MachineType:        "machine-type",
			MinCPUPlatform:     "machine-cpu-platform",
			Image:              "image",
			DiskSize:           10,
			DiskType:           "disk-type",
			Network:            "network",
			Subnetwork:         "subnetwork",
			UseInternalIPOnly:  true,
		},
		Orka: orka.Provider{},
	}

	dir, err := ioutil.TempDir("", t.Name())
	require.NoError(t, err)
	defer os.RemoveAll(dir)

	cfgFile := filepath.Join(dir, "config.toml")

	err = ioutil.WriteFile(cfgFile, []byte(cfg), 0666)
	require.NoError(t, err)

	gotCfg, err := LoadFromFile(cfgFile)
	require.NoError(t, err)
	assert.Equal(t, expectedCfg, gotCfg)
}
