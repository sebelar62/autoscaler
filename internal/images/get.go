package images

import (
	"fmt"
	"os"

	"github.com/bmatcuk/doublestar"
)

func Get(fallback string, allowed []string) (string, error) {
	image := os.Getenv("CUSTOM_ENV_CI_JOB_IMAGE")
	if image == "" {
		return fallback, nil
	}

	if len(allowed) == 0 {
		return image, nil
	}

	for _, allowedImage := range allowed {
		ok, err := doublestar.Match(allowedImage, image)
		if err != nil {
			return "", fmt.Errorf("matching allowed image: %w", err)
		}

		if ok {
			return image, nil
		}
	}

	return "", NewImageNotMatchingPatternError(image)
}
