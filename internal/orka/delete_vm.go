package orka

import (
	"context"
	"net/http"
)

type deleteVMReqBody struct {
	VMID string `json:"orka_vm_name"`
}

func (c *client) DeleteVM(ctx context.Context, id string) error {
	endpoint := getVMEndpointRelURL(endpointDelete)
	reqBody := deleteVMReqBody{
		VMID: id,
	}
	return c.sendHTTPRequestWithRetries(ctx, http.MethodDelete, endpoint, reqBody, nil)
}
