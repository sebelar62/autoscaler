package config

import (
	"testing"

	"github.com/BurntSushi/toml"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestConfigMarshal(t *testing.T) {
	data := `
Endpoint = "http://10.0.0.1"
Token = "foo"
BaseImage = "bar"
AllowedImages = ["foo-*", "!bar"]
User = "gitlab"
Password = "secret"
Cores = 8
HTTPTimeout = 60
BootTimeout = 300
[ImageAliases]
  foo = "bar"
`
	expectedCfg := Provider{
		Endpoint:      "http://10.0.0.1",
		Token:         "foo",
		BaseImage:     "bar",
		AllowedImages: []string{"foo-*", "!bar"},
		User:          "gitlab",
		Password:      "secret",
		Cores:         8,
		HTTPTimeout:   60,
		BootTimeout:   300,
		ImageAliases:  map[string]string{"foo": "bar"},
	}

	var cfg Provider
	err := toml.Unmarshal([]byte(data), &cfg)
	require.NoError(t, err)

	assert.Equal(t, expectedCfg, cfg)
}

func TestValidation(t *testing.T) {
	type testCase struct {
		cfg         Provider
		expectedErr string
	}

	testCases := map[string]testCase{
		"empty config": {
			cfg:         Provider{Token: "foo", BaseImage: "foo", User: "foo", Password: "foo", Cores: 3},
			expectedErr: "endpoint is required",
		},
		"no token": {
			cfg:         Provider{Endpoint: "http://1.2.3.4", BaseImage: "foo", User: "foo", Password: "foo", Cores: 3},
			expectedErr: "token is required",
		},
		"no user": {
			cfg:         Provider{Endpoint: "http://1.2.3.4", Token: "foo", BaseImage: "foo", Password: "foo", Cores: 3},
			expectedErr: "user is required",
		},
		"no password": {
			cfg:         Provider{Endpoint: "http://1.2.3.4", Token: "foo", BaseImage: "foo", User: "foo", Cores: 3},
			expectedErr: "password is required",
		},
		"invalid cores": {
			cfg:         Provider{Endpoint: "http://1.2.3.4", Token: "foo", BaseImage: "foo", User: "foo", Password: "foo", Cores: 0},
			expectedErr: "cores must be 3, 4, 6, 8, 12, or 24",
		},
		"endpoint not valid URL": {
			cfg:         Provider{Endpoint: ":", Token: "foo", BaseImage: "foo", User: "foo", Password: "foo", Cores: 3},
			expectedErr: "endpoint URL is not valid: parse \":\": missing protocol scheme",
		},
		"endpoint not http": {
			cfg:         Provider{Endpoint: "https://1.2.3.4", Token: "foo", BaseImage: "foo", User: "foo", Password: "foo", Cores: 3},
			expectedErr: "endpoint should start with http://",
		},
		"invalid allowed project path": {
			cfg:         Provider{Endpoint: "http://1.2.3.4", Token: "foo", BaseImage: "foo", User: "foo", Password: "foo", Cores: 3, BetaAllowedProjectsPath: "/tmp/nope/a/b"},
			expectedErr: "allowed project path stat",
		},
		"valid config": {
			cfg:         Provider{Endpoint: "http://1.2.3.4", Token: "foo", BaseImage: "foo", User: "foo", Password: "foo", Cores: 3},
			expectedErr: "",
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			err := tc.cfg.Validate()

			if tc.expectedErr == "" {
				require.NoError(t, err)
			} else {
				assert.Contains(t, err.Error(), tc.expectedErr)
			}
		})
	}
}
